package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class VideoGamesSerivceTest {
    VideoGamesRepository videoGamesRepository=new VideoGamesRepository();
    RawgDatabaseClient rawgDatabaseClient=new RawgDatabaseClient();
    VideoGamesService videoGamesService=new VideoGamesService(videoGamesRepository,rawgDatabaseClient);
    @Test
    public void getVideoGameByIdTest(){
        Genre genre=new Genre("adventure");
        List<Genre> listGenre=new ArrayList<>();
        listGenre.add(genre);
        VideoGame videoGame=new VideoGame(Long.getLong("1"),"GTA",listGenre);
        videoGamesRepository.save(videoGame);
        assertTrue(videoGamesService.getOneVideoGame(Long.getLong("1")).getName().equals("GTA"));
    }
}
