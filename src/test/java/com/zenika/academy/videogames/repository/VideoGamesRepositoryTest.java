package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;

public class VideoGamesRepositoryTest {
    VideoGamesRepository videoGamesRepository=new VideoGamesRepository();
    @Test
    public void saveVideoGameTest(){
        Genre genre=new Genre("adventure");
        List<Genre> listGenre=new ArrayList<>();
        listGenre.add(genre);
        VideoGame videoGame=new VideoGame(Long.getLong("1"),"GTA",listGenre);
        videoGamesRepository.save(videoGame);
        assertTrue(videoGamesRepository.get(Long.getLong("1")).isPresent());
    }
}
