package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient client;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient client) {
        this.videoGamesRepository = videoGamesRepository;
        this.client = client;
    }

    public List<VideoGame> ownedVideoGames(String genre) {
        List<VideoGame> videoGameList = new ArrayList<>();
        if (genre.equals("")) {
            videoGameList = this.videoGamesRepository.getAll();
        } else {
            videoGameList = this.videoGamesRepository.getByGenre(genre);
        }
        return videoGameList;
    }

    public VideoGame getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id).orElseThrow(() -> new IllegalArgumentException("This videogame does not exist"));
    }

    public VideoGame addVideoGame(String name, String apiKey) {

        VideoGame newGame = client.getVideoGameFromName(name, apiKey).orElseThrow(() -> new IllegalArgumentException("This videogame does not exist"));

        videoGamesRepository.save(newGame);

        return newGame;
    }

    public void removeVideoGame(Long id) {
        videoGamesRepository.removeById(id);
    }

    @PostConstruct
    private void postConstruct() {
        VideoGame newGame = client.getVideoGameFromName("Pubg", "a44a13dd51e548ecb0523505337b4ffe").orElseThrow(() -> new IllegalArgumentException("This videogame does not exist"));
        videoGamesRepository.save(newGame);
        newGame = client.getVideoGameFromName("mike-morasky", "a44a13dd51e548ecb0523505337b4ffe").orElseThrow(() -> new IllegalArgumentException("This videogame does not exist"));
        videoGamesRepository.save(newGame);

    }
}
