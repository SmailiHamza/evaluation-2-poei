package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public List<VideoGame> getByGenre(String genre) {
        List<VideoGame> videoGameList = new ArrayList<>();
        for (Map.Entry<Long, VideoGame> vl : videoGamesById.entrySet()) {
            for (Genre g : vl.getValue().getGenres()) {
                if (g.getName().equalsIgnoreCase(genre)) {
                    videoGameList.add(vl.getValue());
                    break;
                }
            }
        }
        return videoGameList;
    }

    public Optional<VideoGame> get(Long id) {
        return Optional.ofNullable(this.videoGamesById.get(id));
    }

    public void save(VideoGame v) {
        this.videoGamesById.put(v.getId(), v);
    }

    public void removeById(Long id) {
        get(id).orElseThrow(() -> new IllegalArgumentException("This videogame does not exist"));
        this.videoGamesById.remove(id);
    }

}
