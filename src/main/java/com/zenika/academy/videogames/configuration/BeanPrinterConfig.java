package com.zenika.academy.videogames.configuration;


import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanPrinterConfig {
    RawgDatabaseClient rawgDatabaseClient;

    @Bean
    public RawgDatabaseClient rawgDatabaseClient() {
        return new RawgDatabaseClient();
    }
}
