package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService) {
        this.videoGamesService = videoGamesService;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     * <p>
     * Exemple :
     * <p>
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames(@RequestParam(value = "genre", defaultValue = "") String genre) {
        return videoGamesService.ownedVideoGames(genre);
    }

    /**
     * Récupérer un jeu vidéo par son ID
     * <p>
     * Exemple :
     * <p>
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) {
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id);
        if (foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     * <p>
     * Exemple :
     * <p>
     * POST /video-games
     * Content-Type: application/json
     * <p>
     * {
     * "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName, @Value("${video-game.api-key}") String apiKey) {
        return this.videoGamesService.addVideoGame(videoGameName.getName(), apiKey);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException cnae) {
        return new ResponseEntity<>(cnae.getMessage(), HttpStatus.NOT_FOUND);
    }

    @DeleteMapping
    public String removeVideoGameByName(@RequestParam(value = "id") Long id) {
        this.videoGamesService.removeVideoGame(id);
        return "Videogame removed";
    }
}
