# POEI Rennes 3 : Evaluation 2 (Spring, API Rest, Git)

Cette évaluation vous demande de modifier une application existante. Cette application présente les caractéristiques suivantes : 

 * C'est un projet maven
 * C'est une application Spring Boot
 * L'application expose une API Rest.
 
Votre travail est de modifier cette application en fonction de la liste des tâches fournis dans ce document.

Les tâches sont indépendantes, mais elles sont grossièrement triées par ordre de difficulté, ou de manière à minimiser l'impact sur du code déjà produit pour une tâche précédente.

## Livrable et critères d'évaluation

Le dépot git sur lequel se trouve ce document (https://gitlab.com/BenoitAverty/evaluation-2-poei) doit être forké sur votre compte gitlab. Il contient le projet sur lequel vous devez travailler.

 * Chaque tâche de ce document devra faire l'objet d'une branche. Une fois la tâche terminée, vous pourrez merger la branche dans master et **ne pas supprimer la branche**. Merci d'utiliser l'option `--no-ff` de git merge.
 (Exemple : en étant sur master, executer `git merge --no-ff repo-optional`)
 * Si vous souhaitez apporter une modification à une tâche après l'avoir mergé, vous pouvez revenir sur la branche, ajouter un commit et la merger de nouveau. Git sait très bien gérer ce genre de cas (ne pas oublier d'utiliser l'option `--no-ff`)
 * Seules les branches mergées dans master seront prises en compte, il n'y a pas de pénalité à commencer une tâche et à ne pas la finir.
 * Un historique git propre sera valorisé, surtout si les messages de commits sont clairs et correspondent au changement introduit par le commit.
 
## Liste des tâches à effectuer

### Tâches techniques

#### Repository et Optional

la méthode `get` de la classe `VideoGameRepository` retourne une valeur de type `VideoGame`, mais cette valeur peut être null.

Modifier le type de retour de cette méthode pour utiliser un `Optional`. Cette modification devrait avoir un impact sur au moins deux autres classes.

#### Tests unitaires

Le projet manque cruellement de tests automatisés.

 * Créer des tests unitaires pour la classe `VideoGamesRepository`.
 * Créer des tests pour la classe `VideoGamesService`.

On vous laisse le soin de décider quelles classes mocker ou non pour éviter toute instabilité ou lenteur dans les tests unitaires.

#### Bean manquant

Une ou plusieurs classes du projet sont instanciées directement par une autre, sans inversion de contrôle ni utilisation de Spring.

Identifier cette classe ou ces classes et en faire des beans spring. utiliser ces bean au lieu des instances "sauvages"

#### Erreur 500

Une erreur 500 est parfois retournée par l'api lorsqu'on ajoute un jeu vidéo à la collection. Identifier la cause du bug et le corriger.

Exemple de requête qui cause le bug : 

```http request
POST /video-games
Content-Type: application/json

{
  "name": "cqzsegw"
}
```

#### Valeur non configurable

Une des classes du projet contient une valeur "en dur" (qu'on appelle parfois "magic string"). C'est une pratique à éviter.

Identifier cette valeur et la rendre configurable grace à un mécanisme de configuration de Spring.

### Tâches fonctionnelles

#### Initialisation de données

Quand l'application démarre, aucune donnée n'est présente à l'intérieur (il faut ajouter un jeu avant d'avoir quelque chose à récupérer)

On souhaite avoir des données dans le repository quand l'application démarre. Ajouter un mécanisme pour ajouter un ou deux jeux vidéos à la base au démarrage.

Nous n'avons pas vu la façon de faire ça : vous devrez vous documenter sur internet en cherchant comment utiliser l'annotation `@PostConstruct` de spring.

#### Supprimer un jeu

On peut ajouter un jeu à la collection, lister les jeux ou récupérer un jeu via son id.

 Ajouter une fonctionnalité qui permet de supprimer un jeu de la collection (il peut être utile d'ajouter un exemple de requête dans le fichier `examples.http` avant de coder)
 
#### Marquer un jeu comme « terminé »

Ajouter un attribut `finished` à un jeu video et ajouter un moyen de marquer un jeu comme terminé dans l'api.

Attention, ce statut "terminé ne doit pas être perdu si on essaie d'ajouter un jeu vidéo déjà présent dans la collection.

#### Filter les jeux par genre

Dans le endpoint `/video-games` qui permet de lister les jeux, ajouter un paramètre qui permet de filtrer les jeux par genre.

Exmple : 

```http request
GET /video-games?genre=action
```

devra retourner tous les jeux ayant le genre "action", et uniquement ceux-là.

#### BONUS : gestion des "plateformes"

L'API de RAWG.io, qui contient les infos sur les jeux vidéos contient aussi des infos sur les plateformes pour lesquelles les jeux existent (PC, PlayStation, Switch...)

On souhaite inclure cette information dans la classe Jeu Vidéo manipulée par l'API.

Ensuite, l'ajout d'un jeu vidéo devra prendre un paramètre supplémentaire (plateforme), et le jeu ne sera ajouté que si la plateforme spécifiée existe pour le jeu.

Info : cette tâche nécéssite de comprendre le fonctionnement de la classe RawgDatabaseClient. La documentation des classes que vous ne connaissez pas peut être utile. 